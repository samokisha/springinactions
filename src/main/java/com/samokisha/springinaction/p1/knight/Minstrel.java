package com.samokisha.springinaction.p1.knight;

public class Minstrel {

    public void singBeforeQuest() {
        System.out.println("The knight is so brave!");
    }

    public void singAfterQuest() {
        System.out.println("The brave knight did embark on a quest.");
    }
}
