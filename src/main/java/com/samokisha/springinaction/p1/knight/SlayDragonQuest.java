package com.samokisha.springinaction.p1.knight;

public class SlayDragonQuest implements Quest {
    public void embark() {
        System.out.println("Dragon killed!");
    }
}
