package com.samokisha.springinaction.p1.knight;

public interface Knight {
    void embarkOnQuest();
}
