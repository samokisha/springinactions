package com.samokisha.springinaction.p2.springidol;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringIdolApp {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("p2/springidol/springidol.xml");

        Performer duke = ctx.getBean("duke", Performer.class);
        duke.perform();

        Performer kenny = ctx.getBean("kenny", Performer.class);
        kenny.perform();
    }
}
