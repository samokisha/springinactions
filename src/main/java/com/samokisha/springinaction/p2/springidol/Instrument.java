package com.samokisha.springinaction.p2.springidol;

public interface Instrument {
    void play();
}
