package com.samokisha.springinaction.p2.springidol;

public class Auditorium {

    public void turnOnLights() {
        System.out.println("Lights is on");
    }

    public void turnOffLights() {
        System.out.println("Lights is off");
    }
}
