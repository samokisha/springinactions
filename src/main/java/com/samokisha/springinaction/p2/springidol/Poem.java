package com.samokisha.springinaction.p2.springidol;

public interface Poem {
    void recite();
}
