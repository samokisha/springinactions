package com.samokisha.springinaction.p2.springidol;

public interface Performer {
    void perform();
}
