package com.samokisha.springinaction.p2.springidol;

public class Saxophone implements Instrument {
    public void play() {
        System.out.println("TOOT TOOT TOOT");
    }
}
