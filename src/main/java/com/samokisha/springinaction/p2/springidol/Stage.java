package com.samokisha.springinaction.p2.springidol;

public class Stage {

    private Stage() {
    }

    public static Stage getInstance() {
        return StageSingletonHolder.INSTANCE;
    }

    private static class StageSingletonHolder {
        static final Stage INSTANCE = new Stage();
    }
}
